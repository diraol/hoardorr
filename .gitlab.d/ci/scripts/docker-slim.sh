#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# docker-slim.sh: compile docker-slim for our specific architecures and store those as artifacts
# Usage: bash path/to/docker-slim.sh
set -EeufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

if [[ "${DOCKER_SLIM_VERSION:-unset}" == "unset" ]]; then
	while read -r; do
		>/dev/null 2>/dev/null command -v "${REPLY}" || { echo "please install '${REPLY}' or use the image that has it"; exit 42; }
	done <<-EOF
		curl
		jq
	EOF
	# fetching all releases including pre-releases until there are binaries built
	DOCKER_SLIM_VERSION="$(curl --silent "https://api.github.com/repos/docker-slim/docker-slim/releases" | jq -r '.[0].tag_name')"
fi

# clone the source and checkout the tag
git clone 'https://github.com/docker-slim/docker-slim.git'
cd docker-slim || exit
git checkout "${DOCKER_SLIM_VERSION}"
LD_FLAGS="-s -w -X github.com/docker-slim/docker-slim/pkg/version.appVersionTag=$(git describe --tags) -X github.com/docker-slim/docker-slim/pkg/version.appVersionRev=$(git rev-parse HEAD) -X github.com/docker-slim/docker-slim/pkg/version.appVersionTime=$(date -u '+%Y-%m-%d_%I:%M:%S%p')"

# build for all the architectures
IFS=' ' read -r -a MUCHOS_ARCHES <<<"${MUCHOS_ARCHES}"
for os_arch in "${MUCHOS_ARCHES[@]}"; do
	os="${os_arch%%/*}"
	arch="${os_arch##*/}"
	for b in docker-slim docker-slim-sensor; do
		# build with a proper name
		CGO_ENABLED=0 GOOS="${os}" GOARCH="${arch}" go build \
			-ldflags="${LD_FLAGS}" \
			-trimpath \
			-mod vendor \
			-o "${b}-${os}-${arch}" \
			"./cmd/${b}"

		# add shasums
		sha256sum "${b}-${os}-${arch}" > "${b}-${os}-${arch}.sha256"

		# move to proper artifact location for CI to pick them up
		mv "${b}-${os}-${arch}" "${b}-${os}-${arch}.sha256" ../
	done
done
