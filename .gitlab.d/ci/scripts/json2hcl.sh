#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# download json2hcl for our specific architecures and store those as artifacts
# Usage: bash path/to/json2hcl.sh
set -EeufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

git clone 'https://github.com/kvz/json2hcl.git'
cd json2hcl || exit
git checkout "${JSON2HCL_VERSION}"

go get || true	# outside of gopath

IFS=' '; for a in ${MUCHOS_ARCHES}; do
	IFS='/' read -r os arch <<<"${a}"
	GOOS="${os}" GOARCH="${arch}" go build -o "json2hcl-${os}-${arch}" -ldflags="-X main.Version=${JSON2HCL_VERSION}"
	# TODO: strip "json2hcl-${os}-${arch}"
	sha256sum "json2hcl-${os}-${arch}" > "json2hcl-${os}-${arch}.sha256"
done

set +f
mv json2hcl-* ..
