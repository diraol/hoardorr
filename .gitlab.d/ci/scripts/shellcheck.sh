#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# download shellcheck for our specific architecures and store those as artifacts
# Usage: bash path/to/shellcheck.sh
set -EeufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

function getit() {
	wget -qqO /tmp/shellcheck.tar.xz "${SHELLCHECK_URL}"
	tar Jxf /tmp/shellcheck.tar.xz "shellcheck-${SHELLCHECK_VERSION}/shellcheck" --strip-components=1
	mv shellcheck "shellcheck-${SHELLCHECK_ID}"

	echo "${SHELLCHECK_SHA}  shellcheck-${SHELLCHECK_ID}" >| "shellcheck-${SHELLCHECK_ID}.sha256"
	sha256sum -c < "shellcheck-${SHELLCHECK_ID}.sha256"

	rm -rf shellcheck.tar.xz
}

# linux/amd64
SHELLCHECK_URL="https://github.com/koalaman/shellcheck/releases/download/${SHELLCHECK_VERSION}/shellcheck-${SHELLCHECK_VERSION}.linux.x86_64.tar.xz"
SHELLCHECK_SHA="1e8499d6f90481bfa2af7a4dce86dd9bcba5c0fbd88bf4793b34ec679c8bb98a"
SHELLCHECK_ID="linux-amd64"
getit

# linux/arm64
SHELLCHECK_URL="https://github.com/koalaman/shellcheck/releases/download/${SHELLCHECK_VERSION}/shellcheck-${SHELLCHECK_VERSION}.linux.aarch64.tar.xz"
SHELLCHECK_SHA="80875acb4ed37c5f5b5d1186bc8fadecfc7cd3c4ba0fb8bc75dd023343d21441"
SHELLCHECK_ID="linux-arm64"
getit
