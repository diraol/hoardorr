#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# goss.sh: compile goss for our specific architecures and store those as artifacts
# Usage: bash path/to/goss.sh
set -EeufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

if [[ "${GOSS_VERSION:-unset}" == "unset" ]]; then
	while read -r; do
		>/dev/null 2>/dev/null command -v "${REPLY}" || { echo "please install '${REPLY}' or use the image that has it"; exit 42; }
	done <<-EOF
		curl
		jq
	EOF
	GOSS_VERSION="$(curl --silent "https://api.github.com/repos/aelsabbahy/goss/releases/latest" | jq -r '.tag_name')"
fi

# we build them because we need arm64
git clone 'https://github.com/aelsabbahy/goss.git'
cd goss || exit
git checkout "${GOSS_VERSION}"

# shellcheck disable=2183,2086
sed -i "s/^build: .*/build: $(IFS=' '; printf "release\/goss-%s release\/goss-%s" ${MUCHOS_ARCHES//\//-})/" Makefile

export TRAVIS_TAG="${GOSS_VERSION}"

make build

# make artifacts available in the parent dir
set +f
mv release/goss-* ..
