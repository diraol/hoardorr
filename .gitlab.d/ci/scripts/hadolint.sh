#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# download hadolint for our specific architecures and store those as artifacts
# Usage: bash path/to/hadolint.sh
set -EeufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

HADOLINT_VERSION="${HADOLINT_VERSION:-latest}"

function getit() {
	wget -qqO "hadolint-${HADOLINT_ID}" "${HADOLINT_URL}"

	echo "${HADOLINT_SHA}  hadolint-${HADOLINT_ID}" >| "hadolint-${HADOLINT_ID}.sha256"
	sha256sum -c < "hadolint-${HADOLINT_ID}.sha256"
}

# linux/amd64
HADOLINT_URL="https://github.com/hadolint/hadolint/releases/download/${HADOLINT_VERSION}/hadolint-Linux-x86_64"
HADOLINT_SHA="5099a932032f0d2c708529fb7739d4b2335d0e104ed051591a41d622fe4e4cc4"
HADOLINT_ID="linux-amd64"
getit

# linux/arm64
# Rabbithole starts here: https://github.com/hadolint/hadolint/issues/411
# therefore, on arm we just provide wrapper that errs
cat >| hadolint-linux-arm64 <<-EOF
	#!/bin/bash
	echo "Sorry, hadolint is not yet built for arm64, please check your docker images on amd64 arch."
	exit 42
EOF
sha256sum hadolint-linux-arm64 > hadolint-linux-arm64.sha256
