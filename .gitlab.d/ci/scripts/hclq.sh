#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# hclq.sh: compile hclq for our specific architecures and store those as artifacts
# Usage: bash path/to/hclq.sh
set -EeufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

git clone 'https://github.com/teamon/hclq.git'
cd hclq || exit
git checkout "${HCLQ_VERSION}"

IFS=' ' read -r -a MUCHOS_ARCHES <<<"${MUCHOS_ARCHES}"
for os_arch in "${MUCHOS_ARCHES[@]}"; do
	os="${os_arch%%/*}"
	arch="${os_arch##*/}"
	GOOS="${os}" GOARCH="${arch}" make

	mv hclq "hclq-${os}-${arch}"
	# add shasums
	sha256sum "hclq-${os}-${arch}" > "hclq-${os}-${arch}.sha256"

	# move to proper artifact location for CI to pick them up
	mv "hclq-${os}-${arch}" "hclq-${os}-${arch}.sha256" ../
done
