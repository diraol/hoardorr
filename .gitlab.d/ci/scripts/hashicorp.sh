#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# hashicorp.sh: install hashicorp tools
# Usage: bash path/to/hashicorp.sh
set -EeufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

while read -r; do
	>/dev/null 2>/dev/null command -v "${REPLY}" || { echo "please install '${REPLY}'"; exit 42; }
done <<-EOF
	gpg
	join
	parallel
	sha256sum
	unzip
	wget
EOF

function getit() {
	local tool="${1:-unset}"
	local version="${2:-unset}"
	local os="${3:-unset}"
	local arch="${4:-unset}"

	if [[ "unset" == "${1}" || "unset" == "${2}" || "unset" == "${3}" || "unset" == "${4}" ]]; then
		echo "usage: getit tool version os arch"
		exit 42
	fi

	local hashicorp_key="0x51852D87348FFC4C" # see https://www.hashicorp.com/security.html
	local hashicorp_url="https://releases.hashicorp.com"
	local keyservers=("keyserver.ubuntu.com" "keys.gnupg.net" "pgp.mit.edu" "pool.sks-keyservers.net" "subkeys.pgp.net")

	tmp="$(mktemp -d)"

	# fetch gpg key into tmp dir
	local key_received="false"
	for ksrv in "${keyservers[@]}"; do
		if gpg --homedir "${tmp}" --keyserver "${ksrv}" --recv-keys "${hashicorp_key}"; then
			key_received="true"
			break
		fi
	done
	if [[ "true" != "${key_received}" ]]; then
		>&2 echo "error: can't receive any keys"
		return 255
	fi

	# fetch all required files into tmp dir
	if ! wget -qqP "${tmp}" \
			"${hashicorp_url}/${tool}/${version}/${tool}_${version}_SHA256SUMS" \
			"${hashicorp_url}/${tool}/${version}/${tool}_${version}_SHA256SUMS.sig" \
			"${hashicorp_url}/${tool}/${version}/${tool}_${version}_${os}_${arch}.zip"; then
		>&2 echo "error: can't download files from ${hashicorp_url}!"
		return 255
	fi

	# check sigs
	if ! gpg --homedir "${tmp}" --verify "${tmp}/${tool}_${version}_SHA256SUMS.sig"; then
		>&2 echo "error: can't verify gpg signatures"
		return 255
	fi

	# check shasums (sed to normalize paths)
	if ! grep "_${os}_${arch}.zip$" "${tmp}/${tool}_${version}_SHA256SUMS" | sed "s|  |  ${tmp}/|" | sha256sum -c >/dev/null; then
		>&2 echo "error: can't verify shasums"
		return 255
	fi

	# finally, unpack (and try to strip)
	unzip -p "${tmp}/${tool}_${version}_${os}_${arch}.zip" "${tool}" > "${tool}-${os}-${arch}"
	strip "${tool}-${os}-${arch}" || true

	# generate hoardorr shasums
	sha256sum "${tool}-${os}-${arch}" > "${tool}-${os}-${arch}.sha256"

	# cleanup, but don't fail if can't remove the socket file
	# see the https://stackoverflow.com/a/45865627 for alternative solution,
	# which is not suitable for us, as we're not going to pkill gpg-agent if
	# this wrapper is run locally, this is just impolite
	rm -rf "${tmp}" || true
}
export -f getit

# hoard all the tools with version defined
join -j2 \
	<(env | gawk -F '[_=]' '/^HASHICORP_.*_VERSION=/ {printf "%s/%s\n", tolower($2), $4}' | sort ) \
	<(echo "${MUCHOS_ARCHES}" | tr ' ' '\n' | sort ) \
	| sed 's|/| |g;s/^/getit/;' \
	| SHELL="/bin/bash" parallel --no-notice
