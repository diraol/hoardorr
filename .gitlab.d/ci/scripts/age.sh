#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# age.sh: compile age for our specific architecures and store those as artifacts
# Usage: bash path/to/age.sh
set -EeufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

if [[ "${AGE_VERSION:-unset}" == "unset" ]]; then
	while read -r; do
		>/dev/null 2>/dev/null command -v "${REPLY}" || { echo "please install '${REPLY}' or use the image that has it"; exit 42; }
	done <<-EOF
		curl
		jq
	EOF
	# fetching all releases including pre-releases until there are binaries built
	AGE_VERSION="$(curl --silent "https://api.github.com/repos/FiloSottile/age/releases" | jq -r '.[0].tag_name')"
fi

# we build them because we need arm64
git clone 'https://github.com/FiloSottile/age.git'
cd age || exit
git checkout "${AGE_VERSION}"

# build for all the architectures
IFS=' ' read -r -a MUCHOS_ARCHES <<<"${MUCHOS_ARCHES}"
for os_arch in "${MUCHOS_ARCHES[@]}"; do
	os="${os_arch%%/*}"
	arch="${os_arch##*/}"
	GOOS="${os}" GOARCH="${arch}" go build -o . filippo.io/age/cmd/...

	for b in age age-keygen; do
		# try stripping first
		strip "${b}" || true # TODO: arm64 stripping
		# rename properly
		mv "${b}" "${b}-${os}-${arch}"

		# add shasums
		sha256sum "${b}-${os}-${arch}" > "${b}-${os}-${arch}.sha256"

		# move to proper artifact location for CI to pick them up
		mv "${b}-${os}-${arch}" "${b}-${os}-${arch}.sha256" ../
	done
done
